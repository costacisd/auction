namespace Auction.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bet")]
    public partial class Bet
    {
        public Guid BetID { get; set; }

        public DateTime BetTime { get; set; }

        public double BetPrice { get; set; }

        public Guid LotID { get; set; }

        public Guid UserID { get; set; }

        public virtual Lot Lot { get; set; }

        public virtual User User { get; set; }
    }
}
