namespace Auction.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Lot")]
    public partial class Lot
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Lot()
        {
            Bet = new HashSet<Bet>();
            Comment = new HashSet<Comment>();
        }

        public Guid LotID { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        public DateTime StartTime { get; set; }

        [Required]
        public DateTime EndTime { get; set; }

        [Required]
        public double StartPrice { get; set; }

        public double NowPrice { get; set; }

        public double? BuyNowPrice { get; set; }

        [Required]
        public long Step { get; set; }

        [StringLength(128)]
        public string Picture { get; set; }

        public Guid UserID { get; set; }

        public Guid? CategoryID { get; set; }

        public Guid StatusID { get; set; }

        public Guid? WinnerID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bet> Bet { get; set; }

        public virtual Category Category { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comment { get; set; }

        public virtual Status Status { get; set; }

        public virtual User User { get; set; }

        public virtual User Winner { get; set; }
    }
}
