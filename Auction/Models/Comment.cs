namespace Auction.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Comment")]
    public partial class Comment
    {
        public Guid CommentID { get; set; }

        public DateTime CommentTime { get; set; }

        [Column(TypeName = "text")]
        [Required]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        public Guid UserID { get; set; }

        public Guid LotID { get; set; }

        public virtual Lot Lot { get; set; }

        public virtual User User { get; set; }

    }
}
