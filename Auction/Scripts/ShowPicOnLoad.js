﻿function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#uploaded-img').attr('src', e.target.result).removeAttr('hidden');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#upload").change(function () {
    readURL(this);
});