﻿using Auction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Auction.Providers
{
    public class MyRoleProvider : RoleProvider
    {
        public override string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            string[] roles = new string[] { };
            using (AuctionContext db = new AuctionContext())
            {
                // Получаем пользователя
                User user = db.User.FirstOrDefault(u => u.Login == username);
                if (user != null)
                {
                    Role userRole = db.Role.Find(user.RoleID);
                    if (userRole != null)
                        roles = new string[] { user.Role.RoleName };
                }
                return roles;
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            using (AuctionContext db = new AuctionContext())
            {
                // Получаем пользователя
                User user = db.User.FirstOrDefault(u => u.Email == username);
                if (user != null)
                {
                    Role userRole = db.Role.Find(user.RoleID);
                    if (userRole != null && user.Role.RoleName == roleName)
                        return true;                        
                }               
            }
            return false;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}