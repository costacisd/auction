﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Auction.Models;

namespace Auction.Controllers
{
    public class BetController : Controller
    {
        private AuctionContext db = new AuctionContext();

        // GET: Bet/Create
        public ActionResult Create(Guid lotID, double betPrice)
        {
            Bet bet = new Bet();
            bet.BetPrice = betPrice;
            bet.LotID = lotID;
            return PartialView(bet);
        }

        // POST: Bet/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(Bet bet)
        {
            if (ModelState.IsValid)
            {
                User user = db.User.FirstOrDefault(u => u.Login == User.Identity.Name);
                if (user == null)
                {
                    ModelState.AddModelError("", "Войдите чтобы создать комментарий");
                }
                else
                {
                    Lot lot = db.Lot.Find(bet.LotID);
                    if (bet.BetPrice >= lot.NowPrice + lot.Step)
                    {
                        bet.BetID = Guid.NewGuid();
                        bet.BetTime = DateTime.Now;
                        bet.UserID = user.UserID;
                        db.Bet.Add(bet);

                        if (lot.BuyNowPrice != null && bet.BetPrice >= lot.BuyNowPrice)
                        {
                            lot.WinnerID = bet.UserID;
                            lot.StatusID = db.Status.FirstOrDefault(s => s.Name == "Закрыт").StatusID;
                        }
                        lot.NowPrice = bet.BetPrice;
                        db.Entry(lot).State = EntityState.Modified;

                        db.SaveChanges();
                        //return RedirectToAction("Create", new { lotID = bet.LotID, betPrice = bet.BetPrice + lot.Step});
                        return RedirectToAction("Details", "Lots", new { id = bet.LotID});
                    } else
                    {
                        ModelState.AddModelError("", "Неверная цена");
                    }
                }
            }
            return PartialView(bet);
        }
    }
}
