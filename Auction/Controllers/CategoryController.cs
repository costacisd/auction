﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Auction.Models;

namespace Auction.Controllers
{
    public class CategoryController : Controller
    {
        private AuctionContext db = new AuctionContext();
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CategoryList(Guid? id)
        {
            var category = db.Category.Where(c => c.ParentCategoryID == id);
            Dictionary<Category, string> childs = new Dictionary<Category, string>(category.ToList().Count());
            foreach (var cat in category.ToList()) {
                var childCategory = db.Category.Where(c => c.ParentCategoryID == cat.CategoryID);
                if (childCategory.Any()) childs[cat] = cat.CategoryID.ToString();
                else childs[cat] = "lots"; 
            }
            ViewBag.childs = childs;
            if (category.Any()) return PartialView(category.ToList());
            else return RedirectToAction("LotsFind", "Lots", new { key="category", id = id });
        }

        // GET: Category/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Category/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Category/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
