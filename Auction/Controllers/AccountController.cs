﻿using Auction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Net;

namespace Auction.Controllers
{
    public class AccountController : Controller
    {
        AuctionContext db = new AuctionContext();
        [Authorize]
        public ActionResult Index()
        {
            User user = user = db.User.FirstOrDefault(u => u.Login == User.Identity.Name);
            return View(user);
        }
        // GET: Login
        public ActionResult Login()
        {
            return PartialView();
        }
        // POST: Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            System.Diagnostics.Debug.WriteLine("SomeText");
            if (ModelState.IsValid)
            {
                User user = null;
                using (AuctionContext db = new AuctionContext())
                {
                    var pwd = CreateMD5(model.Password);
                    user = db.User.FirstOrDefault(u => u.Login == model.Login && u.Password == pwd);
                    //user = db.User.FirstOrDefault(u => u.Login == model.Login && u.Password == model.Password);

                }
                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(model.Login, true);
                    return RedirectToAction("Index", "Lots");
                }
                else
                {
                    ModelState.AddModelError("", "Пользователя с таким логином или паролем нет");
                }
            }

            return View(model);
        }
        // GET: Register
        public ActionResult Register()
        {
            return View();
        }
        // POST: Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                User user = null;
                using (AuctionContext db = new AuctionContext())
                {
                    user = db.User.FirstOrDefault(u => u.Login == model.Login);
                }
                if (user == null)
                {
                    using (AuctionContext db = new AuctionContext())
                    {
                        var pwd = CreateMD5(model.Password);
                        db.User.Add(new User { UserID = Guid.NewGuid(), Login = model.Login, Email = model.Email, Password = pwd, RoleID = db.Role.FirstOrDefault(r => r.RoleName == "user").RoleID});
                        //db.User.Add(new User { UserID = Guid.NewGuid(), Login = model.Login, Email = model.Email, Password = model.Password });
                        db.SaveChanges();

                        user = db.User.Where(u => u.Login == model.Login && u.Password == pwd).FirstOrDefault();
                        //user = db.User.Where(u => u.Login == model.Login && u.Password == model.Password).FirstOrDefault();
                    }
                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(model.Login, true);
                        return RedirectToAction("Index", "Lots");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Пользователь с таким логином уже существует");
                }
            }

            return View(model);
        }
        public ActionResult Edit(Guid? id)
        {
            User user = db.User.Find(id);
            EditModel em = new EditModel();
            em.Login = user.Login;
            em.Email = user.Email;
            ViewBag.userID = id;
            return View(em);
        }
        // POST: Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditModel model, Guid userID)
        {
            if (ModelState.IsValid)
            {
                User user = null;
                using (AuctionContext db = new AuctionContext())
                {
                    user = db.User.FirstOrDefault(u => u.Login == model.Login);
                }
                if (user == null)
                {
                    using (AuctionContext db = new AuctionContext())
                    {
                        user = db.User.Find(userID);
                        user.Login = model.Login;
                        user.Email = model.Email;
                        if (!string.IsNullOrEmpty(model.Password))
                            user.Password = CreateMD5(model.Password);
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                        FormsAuthentication.SetAuthCookie(model.Login, true);
                        return RedirectToAction("Index", "Account");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Пользователь с таким логином уже существует");
                }
            }
            ViewBag.userID = userID;
            return View(model);
        }
        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Lots");
        }

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}