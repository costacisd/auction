﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Auction.Models;

namespace Auction.Controllers
{
    public class HomeController : Controller
    {
        AuctionContext db = new AuctionContext();
        public ActionResult Index()
        {
            IEnumerable<Status> statuses = db.Status;
            ViewBag.Status = statuses;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}