﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Auction.Models;

namespace Auction.Controllers
{
    public class CommentController : Controller
    {
        private AuctionContext db = new AuctionContext();
        // GET: Comment
        public ActionResult Comments(Guid id)
        {
            var comments = db.Comment.Include(l => l.User).Where(l => l.LotID == id).OrderByDescending(c => c.CommentTime);
            return PartialView(comments.ToList());
        }

        // GET: Comment/Create
        public ActionResult Create(Guid id)
        {
            Comment comment = new Comment();
            comment.LotID = id;
            return PartialView(comment);
        }

        // POST: Comment/Create
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Comment comment)
        {
            if (ModelState.IsValid)
            {
                User user = db.User.FirstOrDefault(u => u.Login == User.Identity.Name);
                if (user == null)
                {
                    ModelState.AddModelError("", "Войдите чтобы создать комментарий");
                }
                else
                {
                    comment.CommentID = Guid.NewGuid();
                    comment.CommentTime = DateTime.Now;
                    comment.UserID = user.UserID;
                    db.Comment.Add(comment);
                    db.SaveChanges();
                    return RedirectToAction("Create", new { id = comment.LotID });
                }
            }
            return PartialView();
        }

        // POST: Comment/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            Comment comment = db.Comment.Find(id);
            var lotID = comment.LotID;
            db.Comment.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Create", new { id = lotID });
        }
    }
}
