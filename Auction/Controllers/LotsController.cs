﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Auction.Models;

namespace Auction.Controllers
{
    public class LotsController : Controller
    {
        private AuctionContext db = new AuctionContext();

        // GET: Lots
        public ActionResult Index()
        {
            var lot = db.Lot.Include(l => l.Category).Include(l => l.Status).Include(l => l.User).Include(l => l.Winner);
            return View(lot.ToList());
        }

        // GET: Lots Find
        public ActionResult LotsFind(string key = "", Guid? id = null)
        {   
            var lot = db.Lot.Include(l => l.Category).Include(l => l.Status).Include(l => l.User).Include(l => l.Winner);
            switch (key)
            {
                case "category":
                    lot = lot.Where(l => l.CategoryID == id).Where(s => s.Status.Name == "Открыт");
                    break;
                case "user":
                    lot = lot.Where(l => l.User.Login == User.Identity.Name);
                    break;
                case "winner":
                    lot = lot.Where(l => l.Winner.Login == User.Identity.Name);
                    break;
                default:
                    lot = lot.Where(s => s.Status.Name == "Открыт");
                    break;
            }
            if (lot.Any()) return PartialView("Lots", lot.ToList());
            else return Content("<h4>Ничего не найдено</h4>");
        }
        [HttpPost]
        public ActionResult LotsSearch(string name)
        {
            System.Diagnostics.Debug.WriteLine(name);
            var lot = db.Lot.Include(l => l.Category).Include(l => l.Status).Include(l => l.User).Include(l => l.Winner).Where(l => l.Title.Contains(name)).Where(s => s.Status.Name == "Открыт"); ;
            return PartialView("Lots", lot.ToList());
        }

        // GET: Lots/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lot lot = db.Lot.Find(id);
            if (lot == null)
            {
                return HttpNotFound();
            }
            var bets = db.Bet.Where(b => b.LotID == id);
            if (bets == null) ViewBag.Bets = 0;
            else ViewBag.Bets = bets.Count();
            return View(lot);
        }
        
        // GET: Lots/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        public ActionResult ShowCategory(Guid? id, string method, Guid? lotID = null )
        {
            var category = db.Category.Where(c => c.ParentCategoryID == id);
            ViewBag.ParentCategoryID = (id!=null) ? db.Category.Find(id).ParentCategoryID : null;
            ViewBag.method = method;
            if (category.Any())
            {
                ViewBag.lotID = lotID;
                return PartialView("ShowCategory", category.ToList());
            }
            else
            {
                if (method == "create") return RedirectToAction("CreateLot", new { id = id });
                else return RedirectToAction("EditLot", new { lotID = lotID, catID = id });
            }
        }

        // GET: Lots/CreateLot
        [Authorize]
        [HttpGet]
        public ActionResult CreateLot(Guid? id)
        {
            Lot lot = new Lot();
            lot.CategoryID = id;
            lot.StatusID = db.Status.FirstOrDefault(s => s.Name == "Открыт").StatusID;
            var category = db.Category.Find(id);
            ViewBag.Category = category.Name;
            ViewBag.ParentCategoryID = category.ParentCategoryID;
            ViewBag.TimeMin = DateTime.Now.AddDays(1).ToString("s");
            ViewBag.TimeMax = DateTime.Now.AddDays(7).ToString("s");
            return View(lot);
        }

        

        // POST: Lots/CreateLot
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Lot lot, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                User user = db.User.FirstOrDefault(u => u.Login == User.Identity.Name);
                if (user == null)
                {
                    ModelState.AddModelError("", "Войдите чтобы создать лот");
                } 
                else
                {
                    lot.LotID = Guid.NewGuid();
                    if (upload != null)
                    {
                        string path = @"\Pictures\" + lot.LotID + @"\";
                        if (!Directory.Exists(Server.MapPath(path))) Directory.CreateDirectory(Server.MapPath(path));
                        path += "img" + Path.GetExtension(upload.FileName);
                        upload.SaveAs(Server.MapPath(path));
                        lot.Picture = path;
                    } else
                    {
                        lot.Picture = @"\Pictures\noimage.png";
                    }
                    lot.StartTime = DateTime.Now;
                    lot.UserID = user.UserID;
                    lot.NowPrice = lot.StartPrice;
                    db.Lot.Add(lot);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Account");
                }                
            }

            var category = db.Category.Find(lot.CategoryID);
            ViewBag.Category = category.Name;
            ViewBag.ParentCategoryID = category.ParentCategoryID;
            ViewBag.TimeMin = DateTime.Now.AddDays(1).ToString("s");
            ViewBag.TimeMax = DateTime.Now.AddDays(7).ToString("s");
            return View(lot);
        }

        // GET: Lots/Edit/5
        [HttpGet]
        public ActionResult Edit(Guid? id)
        {
            ViewBag.LotID = id;
            return View();
        }
        [HttpGet]
        public ActionResult EditLot(Guid? lotID, Guid? catID = null)
        {
            if (lotID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lot lot = db.Lot.Find(lotID);
            if (lot == null)
            {
                return HttpNotFound();
            }
            if (catID != null) lot.CategoryID = catID;
            var category = db.Category.Find(lot.CategoryID);
            ViewBag.Category = category.Name;
            ViewBag.ParentCategoryID = category.ParentCategoryID;
            ViewBag.TimeMin = lot.StartTime.AddDays(1).ToString("s");
            ViewBag.TimeMax = lot.StartTime.AddDays(7).ToString("s");
            ViewBag.TimeNow = lot.EndTime.ToString("s");
            ViewBag.Status = new SelectList(db.Status, "StatusID", "Name");
            if (Request.IsAjaxRequest()) return View(lot);
            return PartialView(lot);
        }

        // POST: Lots/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Lot lot, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null)
                {
                    string path = @"\Pictures\" + lot.LotID + @"\";
                    if (!Directory.Exists(Server.MapPath(path))) Directory.CreateDirectory(Server.MapPath(path));
                    path += "img" + Path.GetExtension(upload.FileName);
                    upload.SaveAs(Server.MapPath(path));
                    lot.Picture = path;
                }
                lot.NowPrice = lot.StartPrice;
                db.Entry(lot).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Account");
            }
            var category = db.Category.Find(lot.CategoryID);
            ViewBag.Category = category.Name;
            ViewBag.ParentCategoryID = category.ParentCategoryID;
            ViewBag.TimeMin = DateTime.Now.AddDays(1).ToString("s");
            ViewBag.TimeMax = DateTime.Now.AddDays(7).ToString("s");
            ViewBag.Status = new SelectList(db.Status, "StatusID", "Name");
            return View(lot);
        }

        // GET: Lots/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lot lot = db.Lot.Find(id);
            if (lot == null)
            {
                return HttpNotFound();
            }
            return PartialView(lot);
        }

        // POST: Lots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Lot lot = db.Lot.Find(id);
            db.Lot.Remove(lot);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
