﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Auction.Models;

namespace Auction.Controllers
{
    [Authorize (Roles = "admin")]
    public class AdminController : Controller
    {
        private AuctionContext db = new AuctionContext();
        // GET: Admin
        public ActionResult Index(string tab = "lots")
        {
            ViewBag.Tab = tab;
            return View();
        }

        public ActionResult Lots()
        {
            var lot = db.Lot.Include(l => l.Category).Include(l => l.Status).Include(l => l.User).Include(l => l.Winner);
            ViewBag.Tab = "lots";
            return PartialView(lot.ToList());
        }

        public ActionResult Users()
        {
            var user = db.User.Include(u => u.Role);
            ViewBag.Tab = "users";
            return PartialView(user.ToList());
        }
        [HttpGet]
        public ActionResult UserEdit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.Roles = new SelectList(db.Role, "RoleID", "RoleName");
            return PartialView(user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserEdit(User user)
        {
            User us = db.User.Find(user.UserID);
            us.RoleID = user.RoleID;
            db.SaveChanges();
            return RedirectToAction("Index", new { tab = "categories" });
        }
        public ActionResult UserDelete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return PartialView(user);
        }

        [HttpPost, ActionName("UserDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult UserDeleteConfirmed(Guid id)
        {
            User user = db.User.Find(id);
            db.User.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index", new { tab = "users" });
        }

        public ActionResult Categories()
        {
            var categories = db.Category.Include(c => c.ParentCategory).OrderBy(c => c.ParentCategoryID);
            ViewBag.Tab = "categories";
            return PartialView(categories.ToList());
        }

        [HttpGet]
        public ActionResult CategoryCreate()
        {
            Category category = new Category();
            ViewBag.Categories = new SelectList(db.Category, "CategoryID", "Name");
            return PartialView(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CategoryCreate(Category category)
        {
            if (ModelState.IsValid)
            {
                category.CategoryID = Guid.NewGuid();
                db.Category.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index", new { tab = "categories" });
            }
            return View(category);
        }
        [HttpGet]
        public ActionResult CategoryEdit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            ViewBag.Categories = new SelectList(db.Category, "CategoryID", "Name");
            return PartialView(category);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CategoryEdit(Category category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { tab = "categories" });
            }
            ViewBag.Categories = new SelectList(db.Category, "CategoryID", "Name");
            return View(category);
        }
        public ActionResult CategoryDelete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return PartialView(category);
        }

        [HttpPost, ActionName("CategoryDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult CategoryDeleteConfirmed(Guid id)
        {
            Category category = db.Category.Find(id);
            db.Category.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index", new { tab = "categories" });
        }
    }
}