namespace Auction.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrateBD : DbMigration
    {
        public override void Up()
        {
            /*CreateTable(
                "dbo.Bet",
                c => new
                    {
                        BetID = c.Guid(nullable: false),
                        BetTime = c.DateTime(nullable: false),
                        BetPrice = c.Double(nullable: false),
                        LotID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.BetID)
                .ForeignKey("dbo.Lot", t => t.LotID)
                .ForeignKey("dbo.User", t => t.UserID)
                .Index(t => t.LotID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Lot",
                c => new
                    {
                        LotID = c.Guid(nullable: false),
                        Title = c.String(nullable: false, maxLength: 128, unicode: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        StartPrice = c.Double(nullable: false),
                        NowPrice = c.Double(nullable: false),
                        BuyNowPrice = c.Double(),
                        Step = c.Long(nullable: false),
                        Picture = c.String(maxLength: 128, unicode: false),
                        UserID = c.Guid(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        StatusID = c.Guid(nullable: false),
                        WinnerID = c.Guid(),
                    })
                .PrimaryKey(t => t.LotID)
                .ForeignKey("dbo.Category", t => t.CategoryID)
                .ForeignKey("dbo.User", t => t.UserID)
                .ForeignKey("dbo.User", t => t.WinnerID)
                .ForeignKey("dbo.Status", t => t.StatusID)
                .Index(t => t.UserID)
                .Index(t => t.CategoryID)
                .Index(t => t.StatusID)
                .Index(t => t.WinnerID);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        CategoryID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 30, unicode: false),
                        ParentCategoryID = c.Guid(),
                    })
                .PrimaryKey(t => t.CategoryID)
                .ForeignKey("dbo.Category", t => t.ParentCategoryID)
                .Index(t => t.ParentCategoryID);
            
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        CommentID = c.Guid(nullable: false),
                        CommentTime = c.DateTime(nullable: false),
                        Text = c.String(nullable: false, unicode: false, storeType: "text"),
                        UserID = c.Guid(nullable: false),
                        LotID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.User", t => t.UserID)
                .ForeignKey("dbo.Lot", t => t.LotID)
                .Index(t => t.UserID)
                .Index(t => t.LotID);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserID = c.Guid(nullable: false),
                        Login = c.String(nullable: false, maxLength: 30, unicode: false),
                        Email = c.String(nullable: false, maxLength: 30, unicode: false),
                        Password = c.String(nullable: false, maxLength: 32, unicode: false),
                        RoleID = c.Guid(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.Role", t => t.RoleID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        RoleID = c.Guid(nullable: false),
                        RoleName = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.RoleID);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        StatusID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 30, unicode: false),
                    })
                .PrimaryKey(t => t.StatusID);
            */
        }
        
        public override void Down()
        {
            /*
            DropForeignKey("dbo.Lot", "StatusID", "dbo.Status");
            DropForeignKey("dbo.Comment", "LotID", "dbo.Lot");
            DropForeignKey("dbo.Lot", "WinnerID", "dbo.User");
            DropForeignKey("dbo.User", "RoleID", "dbo.Role");
            DropForeignKey("dbo.Lot", "UserID", "dbo.User");
            DropForeignKey("dbo.Comment", "UserID", "dbo.User");
            DropForeignKey("dbo.Bet", "UserID", "dbo.User");
            DropForeignKey("dbo.Lot", "CategoryID", "dbo.Category");
            DropForeignKey("dbo.Category", "ParentCategoryID", "dbo.Category");
            DropForeignKey("dbo.Bet", "LotID", "dbo.Lot");
            DropIndex("dbo.User", new[] { "RoleID" });
            DropIndex("dbo.Comment", new[] { "LotID" });
            DropIndex("dbo.Comment", new[] { "UserID" });
            DropIndex("dbo.Category", new[] { "ParentCategoryID" });
            DropIndex("dbo.Lot", new[] { "WinnerID" });
            DropIndex("dbo.Lot", new[] { "StatusID" });
            DropIndex("dbo.Lot", new[] { "CategoryID" });
            DropIndex("dbo.Lot", new[] { "UserID" });
            DropIndex("dbo.Bet", new[] { "UserID" });
            DropIndex("dbo.Bet", new[] { "LotID" });
            DropTable("dbo.Status");
            DropTable("dbo.Role");
            DropTable("dbo.User");
            DropTable("dbo.Comment");
            DropTable("dbo.Category");
            DropTable("dbo.Lot");
            DropTable("dbo.Bet");
            */
        }
    }
}
