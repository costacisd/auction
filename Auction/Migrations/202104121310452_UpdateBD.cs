namespace Auction.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateBD : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "Password", c => c.String(nullable: false, maxLength: 128, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "Password", c => c.String(nullable: false, maxLength: 32, unicode: false));
        }
    }
}
